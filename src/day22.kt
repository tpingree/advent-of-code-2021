import kotlin.math.abs

fun main() {
  val instructions = fileToLines("input/day22.txt")
  val on = mutableSetOf<Triple<Int, Int, Int>>()
  instructions.forEach {
    var pieces = it.split(" ")
    val mode = pieces[0]
    val constraints = pieces[1].split(",")

    val (minX, maxX) = constraints[0].replace("x=", "").split("..")
    val (minY, maxY) = constraints[1].replace("y=", "").split("..")
    val (minZ, maxZ) = constraints[2].replace("z=", "").split("..")

    if (abs(minX.toInt()) <= 50 && abs(maxX.toInt()) <= 50 && abs(minY.toInt()) <= 50 && abs(maxY.toInt()) <= 50 && abs(minZ.toInt()) <= 50 && abs(maxZ.toInt()) <= 50) {
      for (x in minX.toInt()..maxX.toInt()) {
        for (y in minY.toInt()..maxY.toInt()) {
          for (z in minZ.toInt()..maxZ.toInt()) {
            if (mode == "on") {
              on.add(Triple(x,y,z))
            } else {
              on.remove(Triple(x,y,z))
            }
          }
        }
      }
    }
  }

  println(on.size)
}