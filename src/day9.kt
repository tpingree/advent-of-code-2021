fun main() {
  val grid = fileToLines("input/day9.txt").map {
    it.split("")
      .filter { it.isNotEmpty() }
      .map { it.toInt() }
  }

  day9part1(grid)
  day9part2(grid)
}

fun day9part1(grid: List<List<Int>>) {
  println(getLowPoints(grid).sumOf { 1 + grid[it.first][it.second] })
}

fun day9part2(grid: List<List<Int>>) {
  println(getLowPoints(grid).map {
    getBasinSize(grid, it.first, it.second)
  }.toList().sortedDescending().slice(0 until 3).fold(1){acc, i -> acc * i})
}

fun getBasinSize(grid: List<List<Int>>, i: Int,j: Int): Int {
  val toVisit = mutableSetOf(Pair(i,j))
  val visited = mutableSetOf<Pair<Int,Int>>()
  var size = 0

  while (toVisit.isNotEmpty()) {
    val current = toVisit.first()
    toVisit.remove(current)
    val currentHeight = grid[current.first][current.second]
    visited.add(current)
    size++

    for (p in listOf(Pair(1,0), Pair(0,1), Pair(-1,0), Pair(0,-1))) {
      if (p.first + current.first < 0 || p.first + current.first >= grid.size) continue
      if (p.second + current.second < 0 || p.second + current.second >= grid[current.first].size) continue
      val adjacentPair = Pair(current.first+p.first,current.second+p.second)
      val height = grid[adjacentPair.first][adjacentPair.second]
      if (height != 9 && height > currentHeight && !visited.contains(adjacentPair)) {
        toVisit.add(adjacentPair)
      }
    }
  }

  return size
}

fun getLowPoints(grid: List<List<Int>>): List<Pair<Int, Int>> {
  val lowPoints = mutableListOf<Pair<Int, Int>>()

  for (i in grid.indices) {
    for (j in grid[i].indices) {
      val height = grid[i][j]
      if (getAdjacents(grid, i, j).filter { it <= height }.isEmpty()) {
        lowPoints.add(Pair(i,j))
      }
    }
  }

  return lowPoints.toList()
}

fun getAdjacents(grid: List<List<Int>>, i: Int,j: Int): List<Int> {
  val adjacents = mutableListOf<Int>()

  for (p in listOf(Pair(1,0), Pair(0,1), Pair(-1,0), Pair(0,-1))) {
    if (p.first + i < 0 || p.first + i >= grid.size) continue
    if (p.second + j < 0 || p.second + j >= grid[i].size) continue
    adjacents.add(grid[i+p.first][j+p.second])
  }

  return adjacents.toList()
}