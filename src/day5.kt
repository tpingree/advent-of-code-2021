fun main() {
  val lines = fileToLines("input/day5.txt").map {
    var points = it.split(" -> ")
    Line(
      Point(points[0].split(",")[0].toInt(), points[0].split(",")[1].toInt()),
      Point(points[1].split(",")[0].toInt(), points[1].split(",")[1].toInt())
    )
  }

  day5part1(lines)
  day5part2(lines)
}

fun day5part1(lines: List<Line>) {
  println("Part 1: " + lines
    .filter { it.p1.x == it.p2.x || it.p1.y == it.p2.y }
    .flatMap { it.toPoints() }
    .groupingBy { it }
    .eachCount()
    .filter { it.value > 1 }
    .size)
}

fun day5part2(lines: List<Line>) {
  println("Part 2: " + lines
    .flatMap { it.toPoints() }
    .groupingBy { it }
    .eachCount()
    .filter { it.value > 1 }
    .size)
}

data class Point(val x: Int, val y: Int)
data class Line(val p1: Point, val p2: Point) {
  fun toPoints(): List<Point> {
    var points = mutableListOf<Point>()
    var deltaX = p2.x.compareTo(p1.x)
    var deltaY = p2.y.compareTo(p1.y)

    var x = p1.x
    var y = p1.y
    while (x != p2.x || y != p2.y) {
      points.add(Point(x, y))
      x += deltaX
      y += deltaY
    }
    points.add(Point(x, y))

    return points.toList()
  }


}