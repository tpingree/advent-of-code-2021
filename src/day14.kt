fun main() {
  val lines = fileToLines("input/day14-test.txt")
  var polymer = lines[0]
  var insertionRules = lines.subList(2, lines.size).map {
    val pieces = it.split(" -> ")
    pieces[0] to pieces[1]
  }.toMap()

  day14part1(polymer, insertionRules)
  day14part2(polymer, insertionRules)
  day14part2bonus(polymer, insertionRules)
}

fun day14part1(startingPolymer: String, insertionRules: Map<String, String>) {
  var polymer = startingPolymer
  for (i in 1..10) {
    var newPolymer = ""
    for (i in 0 until polymer.length - 1) {
      var pair = polymer.substring(i until i+2)
      newPolymer += pair[0] + insertionRules[pair]!!
    }
    polymer =  newPolymer + polymer[polymer.length - 1]
  }

  val counts = polymer.split("")
    .filter{it.isNotEmpty()}
    .groupingBy { it }
    .eachCount()
    .map {it.value}

  println(counts.maxOrNull()!! - counts.minOrNull()!!)
}

fun day14part2(polymer: String, insertionRules: Map<String, String>) {
  var frequencies = mutableMapOf<String, Long>()
  for (i in 0 until polymer.length - 1) {
    val pair = polymer.substring(i until i+2)
    frequencies[pair] = frequencies.getOrDefault(pair, 0) + 1
  }

  for (i in 1..40) {
    var newFrequencies = mutableMapOf<String, Long>()
    for (i in frequencies) {
      val letter = insertionRules[i.key]!!
      newFrequencies[i.key[0] + letter] = newFrequencies.getOrDefault(i.key[0] + letter, 0) + i.value
      newFrequencies[letter + i.key[1]] = newFrequencies.getOrDefault(letter + i.key[1], 0) + i.value
    }
    frequencies = newFrequencies
  }

  var elements = mutableMapOf<String, Long>()
  frequencies.forEach {
    elements[it.key[0].toString()] = elements.getOrDefault(it.key[0].toString(), 0L) + it.value
    elements[it.key[1].toString()] = elements.getOrDefault(it.key[1].toString(), 0L) + it.value
  }
  elements[polymer[0].toString()] = elements[polymer[0].toString()]!! + 1
  elements[polymer[polymer.length - 1].toString()] = elements[polymer[polymer.length - 1].toString()]!! + 1
  elements = elements.map{it.key to it.value / 2}.toMap().toMutableMap()
  println(elements.maxOf { it.value } - elements.minOf { it.value })
}

fun day14part2bonus(polymer: String, insertionRules: Map<String, String>) {
  val m1 = recursePolymer(0, insertionRules, "N", "N")
  val m2 = recursePolymer(0, insertionRules, "N", "C")
  m2.forEach { (k, v) -> m1.merge(k, v) { a, b -> a + b } }
  val m3 = recursePolymer(0, insertionRules, "C", "B")
  m3.forEach { (k, v) -> m1.merge(k, v) { a, b -> a + b } }
  m1["N"] = m1["N"]!! + 1
  m1["B"] = m1["B"]!! + 1

  println(m1.map{it.key to it.value / 2}.toMap())
}

fun recursePolymer(level: Int, insertionRules: Map<String, String>, i: String, j: String): MutableMap<String, Int> {
  return if (level == 10) {
    val m1 = mutableMapOf(i to 1)
    mutableMapOf(j to 1).forEach { (k, v) -> m1.merge(k, v) { a, b -> a + b } }
    m1
  } else {
    val m1 = recursePolymer(level + 1, insertionRules, i, insertionRules[i + j]!!)
    recursePolymer(level + 1, insertionRules, insertionRules[i + j]!!, j).forEach { (k, v) -> m1.merge(k, v) { a, b -> a + b } }
    m1
  }
}