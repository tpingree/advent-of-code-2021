fun main() {
  val lines = fileToLines("input/day25.txt")
  val height = lines.size
  val width = lines[0].length

  val cucumbers = mutableListOf<Cucumber>()
  val locations = mutableSetOf<Pair<Int, Int>>()

  for (y in 0 until height) {
    for (x in 0 until width) {
      val c = lines[y][x]

      if (c != '.') {
        cucumbers.add(Cucumber(x, y, if (c == '>') 1 else 0, if (c == 'v') 1 else 0))
        locations.add(Pair(x, y))
      }
    }
  }

  var steps = 1
  while (move(width,height, cucumbers, locations)) steps++
  printCucumbers(width, height, cucumbers)
  println(steps)
}

fun move(width: Int, height: Int, cucumbers: MutableList<Cucumber>, locations: MutableSet<Pair<Int, Int>>): Boolean {
  var moved = false

  listOf(Pair(1,0), Pair(0, 1)).forEach {movement ->
    cucumbers.filter { it.dx == movement.first && it.dy == movement.second && !locations.contains(it.target(width, height)) }.forEach {
      val target = it.target(width, height)
      locations.remove(Pair(it.x, it.y))
      locations.add(target)
      it.x = target.first
      it.y = target.second
      moved = true
    }
  }

  return moved
}

class Cucumber (var x: Int, var y: Int, var dx: Int, var dy: Int) {
  fun target(width: Int, height: Int): Pair<Int, Int> {
    val destX = if (x + dx == width) 0 else x + dx
    val destY = if (y + dy == height) 0 else y + dy
    return Pair(destX, destY)
  }
}

fun printCucumbers(width: Int, height: Int, cucumbers: MutableList<Cucumber>) {
  for (y in 0 until height) {
    for (x in 0 until width) {
      val c = cucumbers.firstOrNull { it.x == x && it.y == y }
      print(if (c != null) if (c.dx > 0) '>' else 'v'  else '.')
    }
    println()
  }
  println()
}