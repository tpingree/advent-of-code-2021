fun main() {
  val lines = fileToLines("input/day15-simple.txt")
  val cavern = lines.map {
    it.split("").filter{it.isNotEmpty()}.map {it.toInt()}.toMutableList()
  }.toMutableList()
  goDijkstra(cavern, false)
  goDijkstra(cavern, true)
}

fun goDijkstra(rawCaverns: MutableList<MutableList<Int>>, expand: Boolean) {
  val caverns: List<List<Int>>

  if (!expand) {
    caverns = rawCaverns
  } else {
    caverns = listOf()
  }

  val dangerMap = mutableMapOf<Pair<Int,Int>, DangerNode>()
  for (i in caverns.indices) for (j in caverns[i].indices) dangerMap[Pair(i,j)] = DangerNode(Pair(i,j), false, caverns[i][j], Integer.MAX_VALUE)
  val dangerList = dangerMap.values.toList()
  val destination =  dangerMap[Pair(caverns.size - 1,caverns[0].size - 1)]!!
  dangerMap[Pair(0,0)]!!.distance = 0
  var current: DangerNode

  do {
    current = dangerList.sortedBy { it.distance }.first {!it.visited}
    current.visited = true
    val neighbors = neighbors(caverns, dangerMap, current)
    neighbors.forEach {
      it.distance = Math.min(it.distance, current.distance + it.cost)
    }
  } while(!destination.visited && dangerList.any { !it.visited })
  println("Final cost: ${destination.distance}")
}

class DangerNode(val cavern: Pair<Int,Int>, var visited: Boolean, var cost: Int, var distance: Int) {
  override fun toString() = "$cavern-$visited"
}

fun neighbors(caverns: List<List<Int>>, dangers: Map<Pair<Int,Int>, DangerNode>, danger: DangerNode): List<DangerNode> {
  return listOf(Pair(-1, 0), Pair(1,0), Pair(0, -1), Pair(0, 1))
    .filter {
      (danger.cavern.first + it.first >= 0)
        && (danger.cavern.first + it.first < caverns.size)
        && (danger.cavern.second + it.second >= 0)
        && (danger.cavern.second + it.second < caverns[0].size)
    }
    .map {dangers[Pair(danger.cavern.first + it.first, danger.cavern.second + it.second)]!!}
    .filter {!it.visited}

}

fun goRecurseBadley(cavern: List<List<Int>>, i: Int, j: Int, visited: MutableSet<Pair<Int, Int>>): Int {
  if (i == cavern.size - 1 && j == cavern[0].size - 1) return 0

  val deltas = listOf(Pair(-1, 0), Pair(1,0), Pair(0, -1), Pair(0, 1))
  var lowestRisk = Int.MAX_VALUE

  deltas.forEach {
    var dest = Pair(i + it.first, j + it.second)
    if (!visited.contains(dest) && (dest.first >= 0) && (dest.first < cavern.size) && (dest.second >= 0) && (dest.second < cavern[0].size)) {
      visited.add(dest)
      val check = goRecurseBadley(cavern, dest.first, dest.second, visited)
      println("Checking $dest -> $check")
      if (check != Int.MAX_VALUE) {
        lowestRisk = (check + cavern[i][j]).coerceAtMost(lowestRisk)
      }
      visited.remove(dest)
    }
  }
  return lowestRisk
}