import java.io.File

fun main() {
  day4part1()
  day4part2()
}

fun day4part1() {
  var input = fileToLines("input/day4.txt")
  var numbersToCall = input[0].split(",").map {it.toInt()}
  var boards = mutableListOf<Board>()

  var i = 2
  while (i < input.size) {
    boards.add(Board(input.slice(i..i+4)))
    i += 6
  }

  for (e in numbersToCall) {
    for (board in boards) {
      if (board.call(e)) {
        println(board.rows)
        println(board.score() * e)
        return
      }
    }
  }
}

fun day4part2() {
  var input = fileToLines("input/day4.txt")
  var numbersToCall = input[0].split(",").map {it.toInt()}
  var boards = mutableListOf<Board>()

  var i = 2
  while (i < input.size) {
    boards.add(Board(input.slice(i..i+4)))
    i += 6
  }

  for (e in numbersToCall) {
    var iterator = boards.iterator()
    while (iterator.hasNext()) {
      var board = iterator.next()
      if (board.call(e)) {
        iterator.remove()
      }

      if(boards.isEmpty()) {
        println(board.rows)
        println(board.score() * e)
        return
      }
    }
  }
}

class Board {
  var rows = listOf(
    mutableListOf(0,0,0,0,0),
    mutableListOf(0,0,0,0,0),
    mutableListOf(0,0,0,0,0),
    mutableListOf(0,0,0,0,0),
    mutableListOf(0,0,0,0,0)
  )

  var marked = listOf(
    mutableListOf(false, false, false, false, false),
    mutableListOf(false, false, false, false, false),
    mutableListOf(false, false, false, false, false),
    mutableListOf(false, false, false, false, false),
    mutableListOf(false, false, false, false, false)
  )

  constructor(board: List<String>) {
    for (i in board.indices) {
      var raw = board[i].trim().split("[ ]+".toRegex())
      for (j in raw.indices) {
        rows[i][j] = raw[j].toInt()
      }
    }
  }

  fun call(called: Int): Boolean {
    for (i in rows.indices) {
      for (j in rows[i].indices) {
        if (rows[i][j] == called) {
          marked[i][j] = true
        }
      }
    }

    return bingo()
  }

  fun bingo(): Boolean {
    for (i in 0 until 5) {
      if (marked[0][i] && marked[1][i] && marked[2][i] && marked[3][i] && marked[4][i]) {
        return true;
      }

      if (marked[i][0] && marked[i][1] && marked[i][2] && marked[i][3] && marked[i][4]) {
        return true;
      }
    }

    return false
  }

  fun score(): Int {
    var score = 0

    for (i in rows.indices) {
      for (j in rows[i].indices) {
        score += if (!marked[i][j]) rows[i][j] else 0
      }
    }

    return score
  }
}