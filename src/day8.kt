fun main() {
  day9part1(fileToLines("input/day8.txt"))
  day9part2(fileToLines("input/day8.txt"))
}

fun day9part1(lines: List<String>) {
  println(lines.map {
    val pieces = it.split(" | ")
    val digits = pieces[1].split(" ")
    digits.filter {arrayOf(2,4,3,7).contains(it.length) }.size
  }.sum())
}

fun day9part2(lines: List<String>) {
  println(lines.sumOf { analyze(it) })
}

fun analyze(line: String): Int {
  val pieces = line.split(" | ")
  val digits = pieces[0].split(" ")
  val segments = digits.map {
    it.split("").filter{ it.isNotEmpty() }.toSet()
  }

  val one = segments.first { it.size == 2 }
  val three = findThree(segments)
  val four = segments.first { it.size == 4 }
  val seven = segments.first { it.size == 3 }
  val eight = segments.first { it.size == 7 }
  val six = findSix(segments, eight.subtract(seven))
  val topLeft = four.subtract(three).first()
  val bottomLeft = eight.subtract(three).subtract(setOf(topLeft)).first()
  val two = segments.first { it.size == 5 && it != three && it.contains(bottomLeft) }
  val five = segments.first { it.size == 5 && it != three && it != two }
  val zero = segments.first { it.size == 6 && it != six && it.contains(bottomLeft) }
  val nine = segments.first { it.size == 6 && it != six && it != zero }

  val digitMap = mapOf(zero to 0, one to 1, two to 2, three to 3, four to 4, five to 5, six to 6, seven to 7, eight to 8, nine to 9)
  val answerDigits = pieces[1].split(" ")
  return answerDigits.map{ digitMap[it.split("").filter{ it.isNotEmpty() }.toSet()] }.joinToString("").toInt()
}

fun findThree (segments: List<Set<String>>): Set<String> {
  val fivers = segments.filter { it.size == 5 }
  if (fivers[0].intersect(fivers[1]).size == 4 && fivers[0].intersect(fivers[2]).size == 4) return fivers[0]
  if (fivers[1].intersect(fivers[0]).size == 4 && fivers[1].intersect(fivers[2]).size == 4) return fivers[1]
  return fivers[2]
}

fun findSix (segments: List<Set<String>>, eightMinusSeven: Set<String>): Set<String> {
  return segments.first { it.size == 6 && it.intersect(eightMinusSeven).size == 4 }
}