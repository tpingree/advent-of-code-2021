import java.io.File

fun main() {
  var fishes = File("input/day6.txt").readText().trim().split(",").map{ it.toInt() }
  day6part1(fishes)
  day6part2(fishes)
}

fun day6part1(raw: List<Int>) {
  var fishes = raw

  for (i in 1 .. 80) {
    fishes = fishes.map { it - 1 }
    fishes = listOf(fishes, fishes.filter { it == -1 }.map { 8 }).flatten()
    fishes = fishes.map { if (it == -1) 6 else it }
  }

  println(fishes.size)
}

fun day6part2(raw: List<Int>) {
  var fishes = raw.groupingBy { it }.eachCount().mapValues { it.value.toLong() }.toMutableMap()
  for (i in -1..8) fishes.putIfAbsent(i, 0L)

  var temp = mutableMapOf(-1 to 0L, 0 to 0L, 1 to 0L, 2 to 0L, 3 to 0L, 4 to 0L, 5 to 0L, 6 to 0L, 7 to 0L, 8 to 0L)
  for (day in 1 .. 256) {
    for (i in -1..7) {
      temp[i] = fishes[i + 1]!!
    }

    var producing = temp[-1]!!
    temp[-1] = 0
    temp[6] = temp[6]!! + producing
    temp[8] = producing
    fishes = temp
  }

  println(fishes.values.sum())
}

