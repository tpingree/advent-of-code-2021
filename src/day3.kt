import java.io.File

fun analyze(lines: List<String>): IntArray {
  val arr = IntArray(lines.get(0).length)
  lines.forEach {
    for (i in it.indices) {
      arr[i] = arr[i] + (if(it[i] == '0') -1 else 1)
    }
  }
  return arr
}

fun fileToLines(filename: String): List<String> {
  var lines = mutableListOf<String>()
  File(filename).forEachLine { lines.add(it) }
  return lines.toList()
}

fun main() {
  day3part1()
  day3part2()
}

fun day3part1() {
  var arr = analyze(fileToLines(("input/day3.txt")))
  var gamma = arr.joinToString("") { if (it > 0) "1" else "0" }
  var epsilon = arr.joinToString("") { if (it > 0) "0" else "1" }

  println("Gamma:   $gamma")
  println("Epsilon: $epsilon")
  println(Integer.parseUnsignedInt(gamma, 2) * Integer.parseUnsignedInt(epsilon, 2))
  println()
}

fun day3part2() {
  var input = "input/day3.txt"
  var o2rating = fileToLines(input)
  var co2rating = fileToLines(input)

  var pos = 0;
  while (o2rating.size > 1) {
    var arr = analyze(o2rating)
    o2rating = o2rating.filter {
      it[pos] == if (arr[pos] >= 0)  '1' else '0'
    }
    pos++
  }

  pos = 0;
  while (co2rating.size > 1) {
    var arr = analyze(co2rating)
    co2rating = co2rating.filter {
      it[pos] == if (arr[pos] >= 0)  '0' else '1'
    }
    pos++
  }

  println("O2 rating:  ${o2rating[0]}")
  println("CO2 rating: ${co2rating[0]}")
  println(Integer.parseUnsignedInt(o2rating[0], 2) * Integer.parseUnsignedInt(co2rating[0], 2))
}