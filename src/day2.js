const { readFileSync } = require('fs');
const orders = readFileSync('day2-input.txt').toString().trim().replace(/\r\n/g,'\n').split('\n');

function followOrders(label, orders, followOrder) {
  let state = {depth: 0, horizontal_position: 0, aim: 0};

  orders.forEach(order => {
    let action = order.split(" ")[0];
    let delta = parseInt(order.split(" ")[1]);
    state = followOrder(state, action, delta);
  });

  console.log(label + " ------------------");
  console.log(" * Depth:    " + state.depth);
  console.log(" * Position: " + state.horizontal_position);
  console.log(" * Answer:   " + (state.depth * state.horizontal_position))
  console.log()
}

followOrders("Part 1", orders, (state, action, delta) => ({
  depth:                state.depth               + (["up", "down"].includes(action) ? (action == 'up' ? -delta : delta): 0),
  horizontal_position:  state.horizontal_position + (action == 'forward' ? delta : 0),
  aim:                  state.aim
}));

followOrders("Part 2", orders, (state, action, delta) => ({
  depth:                state.depth               + (action == 'forward' ? state.aim * delta : 0),
  horizontal_position:  state.horizontal_position + (action == 'forward' ? delta : 0),
  aim:                  state.aim                 + (["up", "down"].includes(action) ? (action == 'up' ? -delta : delta): 0)
}));
