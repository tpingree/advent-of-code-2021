fun main() {
  var (points, instructions) = linesToPointsAndInstructions()
  for (i in instructions.indices) {
    points = fold(points, instructions[i])
    if (i == 0) println(points.size)
  }

  var startX = points.minOfOrNull { it.first }!!
  var endX = points.maxOfOrNull { it.first }!!
  var startY = points.minOfOrNull { it.second }!!
  var endY = points.maxOfOrNull { it.second }!!

  for (y in startY..endY) {
    for (x in startX..endX) {
      print(if (points.contains(Pair(x,y))) "#" else ".")
    }
    println()
  }
}

fun linesToPointsAndInstructions(): Pair<MutableSet<Pair<Int,Int>>, List<Pair<String, Int>>> {
  val lines = fileToLines("input/day13.txt")
  val points = lines
   .filter{it.isNotEmpty() && !it.startsWith("fold")}
   .map {
    val pieces = it.split(",")
    Pair(pieces[0].toInt(), pieces[1].toInt())
  }.toMutableSet()

  val instructions = lines
    .filter{it.isNotEmpty() && it.startsWith("fold")}
    .map {
      val pieces = it.replace("fold along ","").split("=")
      Pair(pieces[0], pieces[1].toInt())
    }

  return Pair(points, instructions)
}

fun fold(points: MutableSet<Pair<Int,Int>>, instruction: Pair<String, Int>): MutableSet<Pair<Int,Int>> {
  var newPoints = points.filter{ (instruction.first == "x" && it.first != instruction.second) || (instruction.first == "y" && it.second != instruction.second) }.toMutableSet()
  var stationary = newPoints.filter{ (instruction.first == "x" && it.first < instruction.second) || (instruction.first == "y" && it.second < instruction.second) }.toMutableSet()
  var moving = newPoints.filter{ (instruction.first == "x" && it.first > instruction.second) || (instruction.first == "y" && it.second > instruction.second) }.toMutableSet()

  moving.forEach{
    var newPoint =Pair(
      if (instruction.first == "x") it.first - (2 * (it.first - instruction.second)) else it.first,
      if (instruction.first == "y") it.second - (2 * (it.second - instruction.second)) else it.second
    )
    stationary.add(newPoint)
  }

  return stationary
}