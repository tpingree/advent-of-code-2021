fun main() {
  var octopodes = octopodes()
  var flashes = 0
  for (i in 1..100) {
    flashes += step(octopodes)
  }
  println(flashes)

  octopodes = octopodes()
  var step = 0
  do {
    step++
    flashes = step(octopodes)
  } while (flashes != 100)
  println(step)
}

fun octopodes(): List<List<Octopus>> {
  var octopodes = fileToLines("input/day11.txt").map {
    it.split("").filter{it.isNotEmpty()}.map{Octopus(it.toInt(), false, mutableListOf<Octopus>())}
  }

  for (i in octopodes.indices) {
    for (j in octopodes[i].indices) {
      for (di in listOf(-1,0,1)) {
        for (dj in listOf(-1,0,1)) {
          if (di == 0 && dj == 0) continue;
          if ((di + i < 0) || (di + i >= octopodes.size)) continue
          if ((dj + j < 0) || (dj + j >= octopodes[i].size)) continue
          octopodes[i][j].neighbors.add(octopodes[i + di][j + dj])
        }
      }
    }
  }

  return octopodes
}

fun step(octopodes: List<List<Octopus>>): Int {
  var all = octopodes.flatMap{it}

  all.forEach { it.value++ }
  while (all.any { it.value > 9 && !it.flashed }) {
    all
      .filter { it.value > 9 && !it.flashed }
      .forEach {
        it.flashed = true
        it.neighbors.forEach {
          it.value++
        }
      }
  }

  val flashed = all.filter { it.flashed }.size
  all
    .filter { it.flashed }
    .forEach {
      it.flashed = false
      it.value = 0
    }

  return flashed
}

class Octopus (var value: Int, var flashed: Boolean, val neighbors: MutableList<Octopus>) {
  override fun toString(): String {
    return "($value,$flashed)"
  }
}