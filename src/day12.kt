fun main() {
  val (start, end, caves) = linesToCaves(fileToLines("input/day12.txt"))

  // Part 1
  var finishedPaths = mutableSetOf<Path>()
  var paths = listOf(Path(mutableListOf(start)))
  while (paths.isNotEmpty()) {
    paths = paths.flatMap {it.next()}
    finishedPaths.addAll(paths.filter {it.caves[it.caves.size - 1] == end})
    paths = paths.filter{it.caves[it.caves.size - 1] != end}
  }
  println(finishedPaths.size)

  // Part 2
  finishedPaths.clear()
  paths = listOf(Path(mutableListOf(start)))
  while (paths.isNotEmpty()) {
    paths = paths.flatMap {it.nextWith2VisitsToSmallCaves()}
    finishedPaths.addAll(paths.filter {it.caves[it.caves.size - 1] == end})
    paths = paths.filter{it.caves[it.caves.size - 1] != end}
  }
  println(finishedPaths.size)
}

fun linesToCaves(lines: List<String>): Triple<Cave, Cave, List<Cave>> {
  val start = Cave("start", false, mutableSetOf())
  val end = Cave("end", false, mutableSetOf())
  var cavesByName = mutableMapOf("start" to start, "end" to end)

  lines.forEach {
    val pieces = it.split("-")
    cavesByName.putIfAbsent(pieces[0], Cave(pieces[0], pieces[0].contains(Regex("[A-Z]")), mutableSetOf()))
    cavesByName.putIfAbsent(pieces[1], Cave(pieces[1], pieces[1].contains(Regex("[A-Z]")), mutableSetOf()))
  }

  lines.forEach {
    val pieces = it.split("-")
    cavesByName[pieces[0]]!!.neighbors.add(cavesByName[pieces[1]]!!)
    cavesByName[pieces[1]]!!.neighbors.add(cavesByName[pieces[0]]!!)
  }

  return Triple(start, end, cavesByName.values.toList())
}

class Path(val caves: MutableList<Cave>) {
  fun add(cave: Cave): Path {
    val newCaves = ArrayList(caves)
    newCaves.add(cave)
    return Path(newCaves)
  }

  fun next(): List<Path> {
    val next = mutableListOf<Path>()

    val current = caves[caves.size - 1]!!
    current.neighbors.forEach {
      if (it.large || (!it.large && !caves.contains(it))) next.add(add(it))
    }

    return next
  }

  fun nextWith2VisitsToSmallCaves(): List<Path> {
    val next = mutableListOf<Path>()

    val current = caves[caves.size - 1]!!
    current.neighbors.forEach {
      val large = it.large
      val firstTimeThroughARoom = !caves.contains(it)
      val hasNotVisitedSmallRoomTwice = caves.filter{!it.large}.toSet().size == caves.filter{!it.large}.size
      if (it.name != "start" && (large || firstTimeThroughARoom || hasNotVisitedSmallRoomTwice)) next.add(add(it))
    }

    return next
  }

  override fun toString() = caves.joinToString("->") { it.name }
  override fun equals(other: Any?): Boolean {
    if (other == null || other !is Path) return false
    return toString() == other.toString()
  }
}

class Cave(val name: String, val large: Boolean, val neighbors: MutableSet<Cave>) {
  override fun toString() = "@@$name ($large)" + if (neighbors.isNotEmpty()) " -> " + neighbors.joinToString { it.name } else ""
  override fun equals(other: Any?): Boolean {
    if (other == null || other !is Cave) return false
    return name == other.name
  }
}