import java.io.File
import kotlin.math.abs

fun main() {
  var crabs = File("input/day7.txt").readText().trim().split(",").map{ it.toInt() }

  println(calcBestFuel(crabs) {start, end -> abs(start - end)})
  println(calcBestFuel(crabs) {start, end ->
    var n = abs(start - end)
    (n * (n+1)) / 2
  })
}

fun calcBestFuel(crabs: List<Int>, fuelCalculator: (Int, Int) -> Int): Int {
  return IntRange(crabs.minOrNull()!!, crabs.maxOrNull()!!)
    .minOf { destination -> crabs.sumOf { crab -> fuelCalculator(crab, destination) } }
}