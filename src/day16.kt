/*
fun main() {
  val hex = "D2FE28"
  val binary = hexToBinary(hex)

  print(parsePacket(binary))
}

fun parsePacket(binary: String): Pair<Packet, Int> {
  val version = binary.slice(0 until 3).toInt(2)
  val type = binary.slice(3 until 6).toInt(2)

  when(type) {
    4 -> {
      var hasNext: Boolean
      var current = 6
      var number = ""
      do {
        hasNext = binary[current] == '1'
        number += binary.slice(current+1 until current+5)
        current += 5
      } while (hasNext)

      return Packet(version, type, number.toInt(2), listOf())
    }

    else -> {
      if (binary[6] == '0') {
        var length  = binary.slice(7 until 7 + 15).toInt(2)
      } else {
        var count = binary.slice(7 until 7 + 11).toInt(2)
      }

      return Packet(0,0,0, listOf())
    }
  }
}

fun parsePacketsByLength(binary: String, length: Int): List<Packet> {
  val packets = mutableListOf<Packet>()

  return packets.toList()
}

fun parsePacketsByCount(binary: String, count: Int): List<Packet> {
  val packets = mutableListOf<Packet>()

  return packets.toList()
}

data class Packet(val version: Int, val type: Int, val value: Int, val subPackets: List<Packet>)

fun hexToBinary(hex: String): String {
  val hexToBinary = mapOf(
    '0' to "0000", '1' to "0001", '2' to "0010", '3' to "0011",
    '4' to "0100", '5' to "0101", '6' to "0110", '7' to "0111",
    '8' to "1000", '9' to "1001", 'A' to "1010", 'B' to "1011",
    'C' to "1100", 'D' to "1101", 'E' to "1110", 'F' to "1111"
  )
  return hex.map { hexToBinary[it]!! }.joinToString("")
}

 */
