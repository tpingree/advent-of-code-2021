fun main() {
  val lines = fileToLines("input/day10.txt")
  day10part1(lines)
  day10part2(lines)
}

fun day10part1(lines: List<String>) {
  val points = lines.map {
    var points = 0
    val stack = ArrayDeque<String>()
    val chars = it.split("").filter{it.isNotEmpty()}
    val openings = setOf("(", "[", "{", "<")
    val closingsToOpenings = mapOf(")" to "(", "]" to "[", "}" to "{", ">" to "<")
    val pointMap = mapOf(")" to 3, "]" to 57, "}" to 1197, ">" to 25137)

    for (c in chars) {
      if (openings.contains(c)) {
        stack.addLast(c)
      } else {
        val last = stack.removeLast()
        if (last != closingsToOpenings[c]) {
          points = pointMap[c]!!
          break
        }
      }
    }

    points
  }

  println(points.sum())
}

fun day10part2(lines: List<String>) {
  val points = lines.map {
    var points = 0L
    val stack = ArrayDeque<String>()
    val chars = it.split("").filter{it.isNotEmpty()}
    val openings = setOf("(", "[", "{", "<")
    val closingsToOpenings = mapOf(")" to "(", "]" to "[", "}" to "{", ">" to "<")
    val pointMap = mapOf(")" to 3, "]" to 57, "}" to 1197, ">" to 25137)
    val newPointMap = mapOf("(" to 1, "[" to 2, "{" to 3, "<" to 4)

    for (c in chars) {
      if (openings.contains(c)) {
        stack.addLast(c)
      } else {
        val last = stack.removeLast()
        if (last != closingsToOpenings[c]) {
          points = pointMap[c]!!.toLong()
          break
        }
      }
    }

    if (points == 0L) {
      points = 0

      while (stack.isNotEmpty()) {
        points *= 5
        points += newPointMap[stack.removeLast()]!!
      }
    } else {
      points = 0
    }

    points
  }

  var trimmedPoints = points.filter {it > 0}.sorted()
  println(trimmedPoints[(trimmedPoints.size - 1) / 2])
}